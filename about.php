<?php  
/**
 * Template name: About
 * 
 */
get_header(); 
?>
<div id="content" class="site-content">
   <?php influencer_internship_about_page_header(); ?>
   <div id="primary" class="content-area">
       <main id="main" class="site-main">
            <div class="cm-wrapper">
                <div class="about-story">
                    <div class="story-content-wrap">
                        <div class="entry-header">
                        <?php 
                        $story_sub_title   = get_theme_mod('story_sub_title', __( 'Things Behind Our Success','influencer-internship' ) );
                        $story_title       = get_theme_mod('story_title',__( 'Our Story','influencer-internship' ) );
                        ?>
                        <?php if($story_sub_title){?>
                            <h4 class="entry-subtitle"><?php echo esc_html($story_sub_title); ?></h4>
                            <?php } ?>

                        <?php if($story_title ){?>
                            <h2 class="entry-title"><?php echo esc_html($story_title ); ?></h2>
                            <?php } ?>
                        </div>
                        <div class="story-desc">
                            <?php the_content(); ?>
                        </div>
                    </div>
                        <?php if(has_post_thumbnail() ){?>
                        <figure class="story-img">
                            <?php the_post_thumbnail('full'); ?>
                        </figure> 
                        <?php } ?>  
                </div>
            </div>
            <div class="story-feat-wrap clearfix">
                <?php if ( is_active_sidebar('about-sidebar')){
                    dynamic_sidebar('about-sidebar');
                    }
                ?>
                <div class="widget-wrap">
                    <?php if ( is_active_sidebar('about-sidebar-2')){
                        dynamic_sidebar('about-sidebar-2');
                        }
                    ?>
                </div>
            </div>
            <section class="story-statcounter-section" style="background: url(images/stat-counter-bg.jpg) no-repeat;">
             <div class="cm-wrapper">
                 <div class="section-widget-wrap">
                        <?php if( is_active_sidebar('about-sidebar-3')){
                            dynamic_sidebar('about-sidebar-3');
                            }
                        ?>
                  </div>
              </div>
            </section>
            <section class="story-team-section">
                <div class="cm-wrapper">
                    <div class="section-widget-wrap">
                        <?php if( is_active_sidebar('about-sidebar-4')){
                            dynamic_sidebar('about-sidebar-4');
                        }
                        ?>	
                    </div>
                </div>
            </section>  
       </main>
   </div>
</div>
<?php get_footer(); ?>