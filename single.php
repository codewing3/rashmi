<?php get_header();?>
	<div id="content" class="site-content">
		<?php influencer_internship_single_page_header(); ?>
		<div class="cm-wrapper">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<div class="article-group">
						<?php
						if(have_posts() ){
                             while( have_posts() ){
                                   the_post();?>
							<?php get_template_part('template-parts/content','single');?>
						<?php } } ?>
					</div>
					<div class="author-newsletter-wrap">
						<div class="about-author">
							<figure class="author-image">
							   <img src="<?php echo esc_url(get_template_directory_uri() .'/images/post-author-img.jpg'); ?>" alt="<?php esc_attr_e('author image','influencer-internship'); ?>">
							</figure>
							<h3 class="author-name"><span><?php the_author(); ?></span></h3>
							<div class="author-desc">
								<?php the_author_meta('description'); ?>
							</div>
						</div>
						<div class="post-newsletter">
							<img src="<?php echo esc_url( get_template_directory_uri() . '/images/form.jpg')?>" alt="<?php esc_attr_e('form','influencer-internship'); ?>">
						</div>
					</div>
					<div class="related-post">
						<?php influencer_internship_single_page_related_article_section();?>
					</div>
						<?php 
						if ( comments_open() || get_comments_number() ){
							comments_template();
						}
					   ?>
				</main>
			</div>
			<?php if (get_theme_mod('layout_single') === 'single-layout') get_sidebar();?>
		</div>
  </div>
<?php get_footer();?>