<?php get_header();?>
<div id="content" class="site-content">
	<?php influencer_internship_blog_page_header(); ?>
	<div class="cm-wrapper">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<?php if( get_theme_mod('layout')=== 'default-layout'){
					$class = 'grid-layout';
				} else{
					$class = '';
				}?>
				<div class="article-group <?php echo esc_attr($class); ?>">
					<?php if ( have_posts() ){
						while( have_posts() ){
							the_post();
							get_template_part('template-parts/content',get_post_type() );?>
					<?php } } ?>
				</div>
				<?php
				the_posts_pagination( array(
					'prev-next'            => false,
					'before_page_number'   => '<span class="meta-nav screen-reader-text">' . __('Page','influencer-internship'). '</span>',
					) 
				);
				?>
			</main>
		</div>
		<?php if (get_theme_mod('layout') === 'blog1-layout') { get_sidebar();} ?>
	</div>
</div>
<?php get_footer();?>