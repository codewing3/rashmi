
<?php get_header();?>
<div id="content" class="site-content">
     <?php
		  $banner_image =  get_theme_mod('banner_image', get_template_directory_uri() . '/images/single-header-bg.jpg');
		?>
	   <div class="page-header" style="background: url(<?php echo esc_url($banner_image); ?>) no-repeat;">
			<div class="cm-wrapper">
			    <h2 class="page-title"><?php the_archive_title();?></h2> 
				<a href="#primary" class="scroll-down"></a>
		  </div>
        </div>		
	<div class="cm-wrapper">
		<div id="primary" class="content-area">
			<main id="main" class="site-main"> 
				<div class="article-group ">
					<?php if ( have_posts() ){
					 while( have_posts() ){
						the_post();
							get_template_part('template-parts/content', get_post_type() );
						} } ?>
				</div>
				<?php
				the_posts_pagination( array(
					'prev-next'            => false,
					'before_page_number'   => '<span class="meta-nav screen-reader-text">' . __('Page','influencer-internship'). '</span>',
					) 
				);
				?>
			</main>
		</div>
		<?php  get_sidebar(); ?>
	</div>
</div>
<?php get_footer();?>