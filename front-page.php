<?php get_header();?>
  <?php influencer_internship_front_page_header(); ?>
    <section id="clients" class="client-section">
			<div class="cm-wrapper">
				<section class="widget widget_raratheme_client_logo_widget">            
          <?php if( is_active_sidebar('front-page')){
                dynamic_sidebar('front-page');
            }
          ?>
				</section>
			</div>
		</section><!-- .client-section -->
    <section class="about-section">
			<div class="cm-wrapper">               
        <?php if( is_active_sidebar('front-page-2')){
            dynamic_sidebar('front-page-2');
          }
        ?>
			</div>
		</section>
    <section class="service-section">
			<div class="cm-wrapper">
        <?php if( is_active_sidebar('front-page-3')){
              dynamic_sidebar('front-page-3');
            }
          ?>
			</div>
		</section><!-- .service-section -->

    <section class="testimonial-section">
			<div class="cm-wrapper">
				<div class="section-widget-wrap">
          <?php if( is_active_sidebar('front-page-4')){
                dynamic_sidebar('front-page-4');
            }
          ?>
				</div>
			</div>
		</section><!-- .testimonial-section -->

    <section class="cta-section">
			<div class="cm-wrapper">
				<section class="widget widget_raratheme_companion_cta_widget">        
          <?php if( is_active_sidebar('front-page-5')){
                  dynamic_sidebar('front-page-5');
              }
            ?>  
				</section>
			</div>
		</section><!-- .cta-section -->

<?php influencer_internship_front_page_news_section();?>

<?php get_footer();?>