<?php
/**
 * @package influecer-internship
 */

 $themedata = wp_get_theme();

 if( ! defined( '_INFLUENCER_INTERNSHIP')){
  // Replace the version number of the theme on each release.s
    define( '_INFLUENCER_INTERNSHIP',$themedata->get('version'));
 }

// influencer_internship setup
 require get_template_directory(). '/inc/setup.php';

function influencer_internship_scripts(){
  wp_style_add_data( 'influencer-internship-style', 'rtl', 'replace' );
  wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0' );
  wp_enqueue_style( 'influencer-internship-fonts', 'https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700|Source+Sans+Pro:400,400i',false );

  wp_enqueue_style( 'influencer-internship-style', get_stylesheet_uri(), array(),'_INFLUENCER_INTERNSHIP' );

  wp_enqueue_script( 'influncer-internship-counterup', get_template_directory_uri() . '/js/jquery.counterup.js',array('jquery'),'1.0', true );
  wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/waypoints-2.0.3.js', array('jquery'),'2.0.3', true );
  wp_enqueue_script( 'influncer-internship-custom',    get_template_directory_uri() . '/js/custom.js', array('jquery'),'_INFLUNECER_INTERNSHIP', true );

}
add_action( 'wp_enqueue_scripts', 'influencer_internship_scripts');



 // influencer_internship template functions
 require get_template_directory(). '/inc/template-functions.php';

//influencer_internship all the customizers
 require get_template_directory(). '/inc/customizer/customizer.php';
 require get_template_directory(). '/inc/customizer/front-page.php';
 require get_template_directory(). '/inc/customizer/latest-news.php';
 require get_template_directory(). '/inc/customizer/single-page-panels.php';
 require get_template_directory(). '/inc/customizer/layout.php';
 require get_template_directory(). '/inc/customizer/podcast-customizer.php';
 require get_template_directory(). '/inc/customizer/about-page.php';
 require get_template_directory(). '/inc/customizer/blog-page.php';


 //influencer-internship widgets
 require get_template_directory(). '/inc/widgets.php';

 function influencer_internship_custom_post_type() {
 
    $labels = array(
        'name'                => _x( 'Podcasts', 'Post Type General Name', 'influencer-internship' ),
        'singular_name'       => _x( 'podcast', 'Post Type Singular Name', 'influencer-internship' ),
        'menu_name'           => __( 'Podcasts', 'influencer-internship' ),
        'parent_item_colon'   => __( 'Parent podcast', 'influencer-internship' ),
        'all_items'           => __( 'All Podcasts', 'influencer-internship' ),
        'view_item'           => __( 'View podcast', 'influencer-internship' ),
        'add_new_item'        => __( 'Add New podcast', 'influencer-internship' ),
        'add_new'             => __( 'Add New', 'influencer-internship' ),
        'edit_item'           => __( 'Edit podcast', 'influencer-internship' ),
        'update_item'         => __( 'Update podcast', 'influencer-internship' ),
        'search_items'        => __( 'Search podcast', 'influencer-internship' ),
        'not_found'           => __( 'Not Found', 'influencer-internship' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'influencer-internship' ),
    );
             
    $args = array(
        'label'               => __( 'Podcasts', 'influencer-internship' ),
        'description'         => __( 'podcast news and reviews', 'influencer-internship' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // 'taxonomies'          => array( 'genres' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest'        => true,
        'menu_icon'           => 'dashicons-admin-generic',
    );
    register_post_type( 'podcasts', $args );

    $catlabels = array(
      'name'                       => _x( 'categories', 'taxonomy general name' ),
      'singular_name'              => _x( 'category', 'taxonomy singular name' ),
      'search_items'               => __( 'Search categories' ),
      'popular_items'              => __( 'Popular categories' ),
      'all_items'                  => __( 'All categories' ),
      'edit_item'                  => __( 'Edit category' ),
      'update_item'                => __( 'Update category' ),
      'add_new_item'               => __( 'Add New category' ),
      'new_item_name'              => __( 'New category Name' ),
      'separate_items_with_commas' => __( 'Separate categories with commas' ),
      'add_or_remove_items'        => __( 'Add or remove categories' ),
      'choose_from_most_used'      => __( 'Choose from the most used categories' ),
      'menu_name'                  => __( 'categories' ),
    ); 
   
    register_taxonomy('categorypodcasts','podcasts',array(
      'hierarchical'          => false,
      'labels'                => $catlabels,
      'show_ui'               => true,
      'show_in_rest'          => true,
      'show_admin_column'     => true,
      'update_count_callback' => '_update_post_term_count',
      'query_var'             => true,
      'rewrite'               => array( 'slug' => 'category-item' ),
    ));
    }
    add_action( 'init', 'influencer_internship_custom_post_type' );

//create metabox
function influencer_internship_meta_boxes(){
  add_meta_box( 
      'meta-wp',
      __( 'MetaBox Details','influencer-internship' ),
      'influencer_internship_meta_callback',
      'post',
      'side' 
  );
}
add_action( 'add_meta_boxes', 'influencer_internship_meta_boxes' );

function influencer_internship_meta_callback( $post ){
  wp_nonce_field( basename( __FILE__ ), 'meta_nonce' );
  $metakey = get_post_meta( $post->ID,'_metawp',true );
  ?>
  <p>
      <label for="metawp"><?php _e( "postwidth",'influencer-internship' ); ?></label>
      <br />
      <input class="widefat" type="text" name="metawp" value="<?php echo esc_attr( $metakey ); ?>" />
  </p>
  <?php 
}

function influencer_internship_save_meta( $post_id ){
  if ( !isset( $_POST['meta_nonce'] ) || !wp_verify_nonce( $_POST['meta_nonce'], basename( __FILE__ ) ) )
  return;

  // Stop WP from clearing custom fields on autosave
  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )  
  return;

  $metawp =  isset( $_POST['metawp'] ) ? esc_html( $_POST['metawp'] ) : ""; 

  update_post_meta( $post_id,'_metawp',$metawp );

}
add_action( 'save_post','influencer_internship_save_meta' );


