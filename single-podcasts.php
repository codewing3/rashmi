<?php get_header(); ?>
<div id="content" class="site-content">
    <?php influencer_internship_podcasts_header(); ?>
    <div class="cm-wrapper">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="article-group list-layout">
                    <?php if(have_posts() ){
                        while(have_posts() ){
                            the_post();?>
                     <article>
                       <?php $user = wp_get_current_user(); ?>
                       <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID',$post->post_author ) ) ); ?>" class="entry-image">
                       <img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" alt="<?php esc_attr_e('audion author','influencer-internship'); ?>"></a>
                        <div class="entry-content">
                            <header class="entry-header">
                                <div class="entry-meta">
                                    <div class="tag-group">
                                        <span><?php echo esc_html(get_the_date() ); ?></span>
                                    </div>
                                    <div class="article-author">
                                        <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID',$post->post_author ) ) ); ?>" class="url">
                                          <?php the_author(); ?></a>
                                    </div>
                                    <span class="article-author-pos"><?php the_author_meta('description'); ?> </span>
                                </div>
                                <h2 class="entry-title"><?php the_title(); ?></h2>
                            </header>
                            <?php the_excerpt(); ?>
                            <img src="<?php echo esc_url(get_template_directory_uri(). '/images/audio-player.jpg'); ?>" alt="<?php esc_attr_e('audio player','influencer-internship'); ?>">
                        </div>
                    </article>
                    <?php } } ?>
                </div>
            </main>
        </div>
    </div>
    <div class="newsletter-wrap">
        <img src="<?php echo esc_url(get_template_directory_uri() .'/images/form.jpg'); ?>" alt="<?php esc_attr_e('form','influencer-internship'); ?>">
    </div>
</div>
	<?php get_footer();?>
</body>
</html>