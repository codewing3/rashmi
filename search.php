<?php get_header();?>
<div id="content" class="site-content">
	<?php
       $banner_image =  get_theme_mod('banner_image', get_template_directory_uri() . '/images/single-header-bg.jpg');
    ?>
  <div class="page-header" style="background: url(<?php echo esc_url( $banner_image);?>) no-repeat;">
				<div class="cm-wrapper">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<a href="#primary" class="scroll-down"></a>
				</div>
			</div>
			<div class="cm-wrapper">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
                    <div class="search-form-wrap">
                        <h3 class="search-title"><?php echo esc_html__('You are looking for...', 'utralight-internship');?> </h3>
                   </div>
                    <?php get_search_form();?>
						<div class="article-group <?php echo esc_attr($class); ?> ">
							<?php if ( have_posts() ){
								while( have_posts() ){
									the_post();?>
							        <?php get_template_part('template-parts/content',get_post_type() );?>
                         <?php } } ?>
						</div>
					</main>
				</div>
				<?php  get_sidebar(); ?>
			</div>
		</div>
<?php get_footer();?>