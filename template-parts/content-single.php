<article itemscope itemtype="http://schema.org/Article">
<div class="side-social-share">
    <img src="<?php echo esc_url(get_template_directory_uri() .'/images/side-social-share.jpg') ?>" alt="side social">
</div>
<?php the_content();?>
<?php if(has_post_thumbnail()) {
            the_post_thumbnail('small');
    }
    ?>
<div class="tag-share-wrap">
    <div class="tag-block">
        <?php
            $post_tags = get_the_tags();
            if ( $post_tags ) {
                foreach( $post_tags as $tag ) {
                echo $tag->name . '.  '; 
                }
            }  
        ?>
    </div>
    <div class="bottom-share-block">
        <img src="<?php echo esc_url(get_template_directory_uri() . '/images/bottom-share.jpg'); ?>" alt="bottom share">
    </div>
</div>
</article>