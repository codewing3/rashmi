<?php
 global $post;
 $author_id   = $post->post_author;
?>
<article id="post-<?php the_ID();  ?>" <?php post_class(); ?>> 
    <?php if( has_post_thumbnail() ){?>
        <a href="<?php the_permalink(); ?>" class="entry-image" itemprop="thumbnailUrl">
        <?php the_post_thumbnail( 'small');?>
    <?php }?>
    <div class="entry-content">
        <header class="entry-header">
            <h2 class="entry-title" itemprop="headline">
                <a href="<?php the_permalink();?>"><?php the_title();?></a>
            </h2>
            <div class="entry-meta">
                <span class="byline" itemprop="author">
                    <?php esc_html_e('by','influencer-internship'); ?> <span class="author vcard">
                    <a href=" <?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ); ?>" class="url" itemprop="name">
                       <?php echo esc_html(get_the_author_meta('display_name', $author_id) ); ?></a>
                    </span>
                </span>
                <span class="comment-box">
                    <span class="comment-count"><?php echo absint(get_comments_number() );?></span><?php esc_html_e('Comments','influencer-internship');?>
                </span>
           </div>
        </header>
        <?php the_excerpt();?>
        <a href="<?php the_permalink();?>" class="readmore" itemprop="MainEntityOfPage"><?php esc_html_e('Continue Reading','influencer-internship');?></a>
  </div>
</article>