<?php
 if ( post_password_required() ) {
	return;
}
?>
<div class="comments-area" itemscope itemtype="http://schema.org/Comment">
		<h2 class="comment-title">Comments:</h2>
		<ol class="comment-list">
			<?php wp_list_comments( array(
			'avatar_size' => 62,
			'max_depth'   => 5,
			'style'       => 'ol',
			'type'        => 'comment',
			'callback'	  => 'ultralight_internship_comment'
			) );?>
		</ol>
	<?php
      function ultralight_internship_comment($comment, $args, $depth){
		if( 'div' === $args['style' ]){
			$tag ='div';
			$add_below= 'comment';}
			else {
			$tag = 'li';
			$add_below = 'div-comment';
			}
			?>
			<<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
			<?php if ( 'div' != $args['style'] ) {?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php } ?>
		
	<footer class="comment-meta">
		<div class="comment-author vcard">
			<?php if ( $args['avatar_size'] != 0 ){
			echo get_avatar( $comment, $args['avatar_size'] ); 
			}
			printf(
				__( '<b class="fn" itemprop="creator"</b> <span class="says"> Says: </span>','influencer-internship' ),
			get_comment_author_link(),
			get_comment_date(),
			get_comment_time(),
			);
			?>
		</div>
		<?php if ( $comment->comment_approved == '0' ) { ?>
			<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'influencer-internship' ); ?></p>
		</br>
		<?php } ?> 	
	   <div class="reply">
			&rarr;
			<?php comment_reply_link( 
						array_merge( 
							$args, 
							array( 
								'add_below' => $add_below, 
								'depth' => $depth, 
								'max_depth' => $args['max_depth'] 
							) 
						) 
					); 
			?>
	  </div>
	</footer>
	<div class="comment-content">           
		<p class="comment-content" itemprop="commentText"><?php comment_text(); ?></p>        
	</div><!-- .text-holder -->
	<?php if('div' !=$args[ 'style']):?>
	<?php endif;
	}
	?>
	<?php
	if ( ! comments_open() ) : ?>
	<?php if( get_option('comment_registration'&& !$user_ID)) :?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'influencer-internship' ); ?></p>
	<?php
		endif;

		endif;
		comment_form();
	?>
</div>
