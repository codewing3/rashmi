<?php
/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function influencer_internship_body_classes( $classes ) {
        // Adds a class of single rightsidebar to singular pages.
    if( is_single() && get_theme_mod('layout_single') === 'single-layout'  ){
        $classes[] ='rightsidebar';
    }
    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
    $classes[] = 'no-sidebar';
    }
    if( is_single() && get_theme_mod('layout_single') === 'single1-layout'){
        $classes[] ='single-centered-layout full width';
        }
    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
    $classes[] = 'no-sidebar';
    }

    if( is_single() && get_theme_mod('layout_single') === 'single2-layout'){
    $classes[] ='single-fullwidth-layout full width';
    }
    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
    $classes[] = 'no-sidebar';
    }

    if( is_home() || get_theme_mod('layout') === 'default-layout' ) {
        $classes[] ='full-width';
    }
    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'no-sidebar';
    }
    if( is_search() || is_archive() ||  get_theme_mod('layout') === 'blog1-layout'){
        $classes[] ='rightsidebar';
    }
    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
    $classes[] = 'no-sidebar';
    }
	return $classes;
}
add_filter( 'body_class', 'influencer_internship_body_classes' );

 function influencer_internship_header(){?>
      <?php if( is_front_page() ){
          $class = 'header-1';
       }else{
           $class ='';
        } 
    ?>
 <header class="site-header <?php echo esc_attr($class);?> " itemscope itemtype="http://schema.org/WPHeader">
      <div class="header-ticker">
            <span class="ticker-close"><i class="fa fa-close"></i></span>
            <div class="ticker-title-wrap">
                <div class="cm-wrapper">
                    <p class="ticker-title">
                        <?php esc_html_e('The ultimate e-mail marketing course is out now!','influencer-internship'); ?>
                        <a href="#"><?php esc_html_e('Register Today','influencer-internship'); ?></a>
                    </p>
                </div>
            </div>
        </div><!-- .header-ticker -->
        <div class="main-header">
            <div class="cm-wrapper">
                <div class="site-branding">
                    <div class="site-logo">
                        <?php
                        if( function_exists( 'the_custom_logo' )){
                            the_custom_logo();
                        }
                        if ( is_front_page() ) :
                            ?>
                            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                            <?php
                        else :
                            ?>
                            <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                            <?php
                        endif;
                        $influencer_internship_description = get_bloginfo( 'description', 'display' );
                        if ( $influencer_internship_description || is_customize_preview() ) :
                            ?>
                            <p class="site-description"><?php echo $influencer_internship_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
                        <?php endif; ?>
                    </div>	
                </div>
                <div class="nav-wrap">
                    <nav class="main-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                        <button class="toggle-button" type="button">
                            <span class="toggle-bar"></span>
                            <span class="toggle-bar"></span>
                            <span class="toggle-bar"></span>
                        </button>
                        <?php 
                            if(has_nav_menu(  'Primary' ){
                                wp_nav_menu(
                                    array(
                                        'theme_location'  => 'primary',
                                        'menu_id'         => 'Primary',
                                        'menu_class'      => 'nav-menu',
                                    )
                                )
                            })
                        ?>
                    </nav>
                </div>
            </div>
        </div><!-- .main-header -->
    </header><!-- .site-header -->
<?php
}
 
function influencer_internship_front_page_header(){?>
<?php
    $caption_image	=	get_theme_mod( 'caption_image', __(get_theme_file_uri('/images/ban-caption.png') ) );
    ?>
    <div class="banner-section">
        <div class="banner-img">
            <div class="ban-img-holder" style="background: url(<?php echo esc_url( get_header_image() ); ?>) no-repeat;"></div>       
            <?php if( $caption_image ){?>
            <div class="cm-wrapper">
                <img src="<?php echo esc_url( $caption_image ); ?>" alt="<?php esc_html_e('banner caption', 'influencer-internship'); ?>">
            </div>
              <?php } ?>
            <a href="#clients" class="scroll-down"></a>
       </div>
    </div><!-- .banner-section -->
<?php
}

function influencer_internship_blog_page_header(){?>
    <?php
    $blog_banner_image = get_theme_mod('blog_banner_image', get_theme_file_uri('/images/header-bg.jpg') );
    $blog_banner_title = get_theme_mod('blog_banner_title', __( 'Content Marketing Articles','influencer-internship') );
    ?>
    <div class="page-header" style="background: url(<?php echo esc_url( $blog_banner_image);?>) no-repeat;">
        <div class="cm-wrapper">
            <?php if($blog_banner_title){?>
            <h1 class="page-title"><?php echo esc_html($blog_banner_title); ?></h1>
        <?php } ?>
            <a href="#primary" class="scroll-down"></a>
        </div>
    </div>
    <?php
}

function influencer_internship_about_page_header(){?>
  <?php
 $about_banner_image = get_theme_mod('about_banner_image', get_theme_file_uri('/images/header-bg.jpg') );
 $about_banner_title = get_theme_mod('about_banner_title', __( 'About Us','influencer-internship') );
 ?>
<div class="page-header" style="background: url(<?php echo esc_url($about_banner_image);?>) no-repeat;">
    <div class="cm-wrapper">
     <?php if($about_banner_title){?>
        <h1 class="page-title"><?php echo esc_html($about_banner_title); ?></h1>
        <?php } ?>
        <div class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <div id="crumbs">
                <a href="#" class="home_crumb"><?php echo esc_html__('Home', 'influencer-internship'); ?></a> 
                <span class="separator">&#9866;</span> 
                <span class="current"><?php echo esc_html__('About Us', 'influencer-internship'); ?></span>
            </div>
        </div>
    </div>
</div>
<?php
}

function influencer_internship_podcasts_header(){?>
    <?php $podcast_banner_image = get_theme_mod('podcast_banner_image', get_template_directory_uri(). '/images/header-bg.jpg');
            $podcast_banner_title = get_theme_mod('podcast_banner_title', __('podcast','influencer-internship') );
        ?>
        <div class="page-header" style="background: url(<?php echo esc_url( $podcast_banner_image); ?>) no-repeat;">
            <div class="cm-wrapper">
                <?php if($podcast_banner_title){?>
                <h1 class="page-title"><?php echo esc_html($podcast_banner_title); ?> </h1>
                <?php } ?>
                <div class="breadcrumb">
                    <a class="breadcrumb-item" href="<?php the_permalink(); ?>"><?php echo esc_html__('Home','influencer-internship'); ?></a>
                    <span class="seperator"><i class="fa fa-caret-right"></i></span>
                    <span class="breadcrumb-item current"><?php echo esc_html__('Podcast','influencer-internship'); ?></span>
                </div>
                <a href="#primary" class="scroll-down"></a>
            </div>
        </div>
    <?php
}

function influencer_internship_front_page_news_section(){?>
<?php
    $latest_news_title          = get_theme_mod( 'latest_news_title', __( 'Latest Articles','influencer-internship' ) );
    $latest_news_subtitle       = get_theme_mod( 'latest_news_subtitle', __( 'Tips for getting things done','influencer-internship' ) );
    $more_from_blog_title       = get_theme_mod( 'more_from_blog_title', __( 'More From The Blog','influencer-internship' ) );
   ?>
    <section class="news-section">
            <div class="cm-wrapper">
                <?php
                 if( $latest_news_subtitle ){?>
                    <p class="section-subtitle"><?php echo esc_html($latest_news_subtitle); ?></p>
                    <?php } ?>
                   <?php 
                   if( $latest_news_title) {?>
                    <h1 class="section-title"><?php echo esc_html($latest_news_title); ?></h1>
                  <?php } ?>
                    <?php 
                        global $post;
                        $author_id  = $post->post_author;
                    ?>
                    <?php
                $news_query		=	new WP_Query( 
                    array(
                    'post_type'      => 'post',
                    'posts_per_page' => 3,
                    'orderby'        => 'title',
                ));
                if( $news_query->have_posts() ){?>
            <div class="news-block-wrap clearfix">
                    <?php while ( $news_query->have_posts() ){
                    $news_query->the_post();
                    ?>
                <div class="news-block" itemscope itemtype="http://schema.org/Article">
                    <figure class="news-block-img">
                        <a href="<?php the_permalink()?>" itemprop="thumbnailUrl">
                        <?php if( has_post_thumbnail() ){
                            the_post_thumbnail( 'influencer_internship_news');
                        }?>
                    </figure>
                    <div class="news-content-wrap">
                        <h3 class="news-block-title" itemprop="headline">
                            <a href="<?php the_permalink();?>"><?php the_title();?></a>
                        </h3>
                        <div class="entry-meta">
                            <span class="byline" itemprop="author">
                                <?php esc_html_e(   'by','influencer-internship');?>	
                                <a itemprop="name" class="author vcard" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta('ID',$author_id) )); ?>">
                                <?php echo esc_html(get_the_author_meta( 'display_name',$author_id ) );?></a>
                            </span>
                            <span class="comments"><?php echo absint(get_comments_number() );?> 
                              <?php esc_html_e('Comments','influencer-internship');?>
                          </span>
                        </div>
                        <div class="news-block-desc" itemprop="text">
                            <?php the_excerpt();?> 
                        </div>
                        <a href="<?php the_permalink(); ?>" class="readmore" itemprop="mainEntityOfPage"><?php esc_html_e('Continue reading','influencer-internship');?> 
                        <img src="<?php echo esc_url(get_template_directory_uri() . '/images/readmore-arrow.png')?>" alt="<?php esc_html_e('readmore arrow', 'influencer-internship'); ?>"></a>
                    </div>
                </div>
                <?php } wp_reset_postdata();?>
            </div>
            <?php } ?>
                <a class="bttn" href="<?php the_permalink(); ?>"><?php esc_html_e('More from blog','influencer-internship'); ?> </a>    
        </div>
   </section><!-- .news-section -->
<?php
}
 
function influencer_internship_single_page_header(){?>
<?php
    global $post;
    $author_id  = $post->post_author;

    $single_banner_image =  get_theme_mod('single_banner_image', __(get_theme_file_uri('/images/single-header-bg.jpg') ) );
    $singlepage_banner_title =  get_theme_mod('singlepage_banner_title',__( 'Big Questions From Content Marketing World to Ask','influencer-internship' ));
   ?>
   <div class="page-header" style="background: url(<?php echo esc_url($single_banner_image); ?>) no-repeat;">
       <div class="cm-wrapper">
           <?php if( $singlepage_banner_title ){?>
           <h1 class="page-title" itemprop="headline"><?php echo esc_html($singlepage_banner_title ); ?> </h1>
           <?php } ?>			
           <div class="entry-meta">
               <span class="posted-on" itemprop="datePublished dateModified">
                   <?php esc_html_e('on','influencer-internship'); ?> 
                   <a href="#">
                       <time class="updated published"><?php echo esc_html(get_the_date('F j,y') ); ?></time>
                   </a>
               </span>
               <span class="byline" itemprop="author">
                   <?php esc_html_e('by', 'influencer-internship'); ?>
                    <span class="author vcard">
                      <a href=" <?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ); ?>" class="url" itemprop="name">
                       <?php echo esc_html(get_the_author_meta('display_name', $author_id) ); ?></a>
                   </span>
               </span>
               <span class="comment-box">
                   <span class="comment-count"><?php echo absint( get_comments_number()); ?>
                   </span> <?php esc_html_e('Comments','influencer-internship');?>
               </span>
           </div>
           <a href="#primary" class="scroll-down"></a>
       </div>
   </div>
   <?php
}

function influencer_internship_single_page_related_article_section(){?> 
    <h3 class="related-post-title"><?php esc_html_e('Related Articles','influencer-internship');?> </h3>
    <?php
        $args= array(
        'post_type'      => 'post',
        'category_in'    => wp_get_post_categories(get_the_ID()),
        'post__not_in'   => array(get_the_ID()),
        'posts_per_page' => 4,
        'orderby'        => 'title',
        );
        //The Query
        $related_query  = new WP_Query( $args);
        // The Loop
        if( $related_query->have_posts() ){?>
    <div class="related-post-wrap clearfix">
            <?php while (  $related_query->have_posts() ){
                        $related_query->the_post();
            ?>
        <div class="related-post-block">
            <figure>
                <?php if (has_post_thumbnail() ){
                        the_post_thumbnail('influencer-internship_related-article');
                }?> </figure>
            <div class="related-content-wrap">
                <h2 class="related-block-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <a href="<?php the_permalink(); ?>" class="readmore"><?php esc_html_e('Continue reading','influencer-internship');?> <img src="<?php echo esc_url(get_template_directory_uri() . '/images/readmore-arrow.png') ?>" alt="<?php esc_attr_e('readmore arrow','influencer-internship'); ?>"></a>
            </div>
        </div>
        <?php } wp_reset_postdata(); ?>
    </div>
    <?php } ?>
<?php
}