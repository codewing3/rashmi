<?php
function influencer_internship_register_sidebars() {
   
    register_sidebars(
        4,
        array(
            'name'          => esc_html__( 'Footer Area %d','influencer-internship'),
            'id'            => 'footer-area',
            'description'   => esc_html__( 'Add widgets here','influencer-internship'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );

    register_sidebar(
        array(
            'name'          => esc_html__( 'Sidebar','influencer-internship'),
            'id'            => 'sidebar',
            'description'   => esc_html__( 'Add widgets here','influencer-internship'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );

    register_sidebars(
        4,
        array(
            'name'          => esc_html__( 'About Sidebar %d','influencer-internship'),
            'id'            => 'about-sidebar',
            'description'   => esc_html__( 'Add widgets here','influencer-internship'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );

    register_sidebars(
        5,
        array(
            'name'          => esc_html__( 'Front Page %d','influencer-internship'),
            'id'            => 'front-page',
            'description'   => esc_html__( 'Add widgets here','influencer-internship'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );


}
add_action('widgets_init','influencer_internship_register_sidebars');
    