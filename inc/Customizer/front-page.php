
<?php
function influencer_internship_frontpage_customizer_panel( $wp_customize){
    $wp_customize->get_section( 'header_image')->panel	  = 'front_page_settings';

   $wp_customize->get_section( 'header_image')->priority  = 10;
   
   $wp_customize->add_panel( 
    'front_page_settings',
     array(
        'priority'    => 40,
        'capability'  => 'edit_theme_options',
        'title'       => esc_html__( 'Front Page Settings', 'influencer-internship' ),
        'description' => esc_html__( 'Customize Front Page Settings', 'influencer-internship' ),
    ) 
);  

    $wp_customize->add_section(
        'banner_caption',
        array(
         'priority'    => 20,
         'capability'  => 'edit_theme_options',
         'title'       => esc_html__( 'Banner Caption', 'influencer-internship' ),
         'panel'     => 'front_page_settings'
       )
     );

    $wp_customize->add_setting( 
        'caption_image', 
    array(
        'default'                       => get_theme_file_uri('/images/ban-caption.png'),     
        'sanitize_callback'             =>  'esc_url_raw',
    )
  );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'caption_image',
            array(
                'label'      => esc_html__( 'upload image', 'influencer-internship' ),
                'section'    => 'banner_caption',
            )
        )
    );

}
add_action( 'customize_register', 'influencer_internship_frontpage_customizer_panel' );

