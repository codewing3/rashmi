<?php 
function influencer_internship_customize_register(  $wp_customize   ){
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'ultralight_internship_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'ultralight_internship_customize_partial_blogdescription',
			)
		);
	}
	//copyright section
	$wp_customize->add_panel(
		'footer_settings',
		array(
		 'priority'    => 203,
		 'capability'  => 'edit_theme_options',
		 'title'       => esc_html__( 'Footer Settings', 'influencer-internship' ),
	   )
	 );

	$wp_customize->add_section(
        'influencer_internship_copyright',
        array(
            'title'      => esc_html__('Copyright','influencer-internship'),
			'panel'     => 'footer_settings',
        )
    );

    $wp_customize->add_setting( 
        'influencer_internship_copyright', 
    array(
        'default'                       =>  esc_html__('© Copyright 2017, Content Marketing. All Rights Reserved. Theme by Rara Theme. Powered by WordPress.','influencer_internship' ),     
        'sanitize_callback'             =>  'wp_kses_post',
   ));

    $wp_customize->add_control( 
        'influencer_internship_copyright', 
    array(
        'label'         =>esc_html__('title','influencer-internship'),
        'section'       => 'influencer_internship_copyright',
        'type'           => 'textarea',
    ));
}
add_action( 'customize_register','influencer_internship_customize_register');

function influencer_internship_customize_partial_blogname() {
	bloginfo( 'name' );
}

function influencer_internship_customize_partial_blogdescription() {
	bloginfo( 'description' );
}
