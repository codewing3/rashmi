<?php
	function influencer_internship_layout_customizer_panel( $wp_customize ){

$wp_customize->add_panel( 
    'layout_settings', 
    array(
        'title'      => esc_html__( 'Layout Settings', 'influencer-internship' ),
        'priority'   => 30,
        'capability' => 'edit_theme_options',
    )
);

$wp_customize->add_section( 
    'blog_layout',
     array(
        'capability'  => 'edit_theme_options',
        'title'       => esc_html__( 'Blog Layout', 'influencer-internship' ),
        'panel'       => 'layout_settings',
        'priority'    => 20,
    ) 
);

/** Blog layout */
$wp_customize->add_setting( 
    'layout', 
    array(
        'default'           => 'default-layout',
        'sanitize_callback' => 'influencer_internship_sanitize_radio'
    ) 
);

$wp_customize->add_control(
    'layout',
        array(
            'section'     => 'blog_layout',
            'label'       => esc_html__( 'Choose Blog Layout', 'influencer-internship' ),
            'description' => esc_html__( 'This is the general layout for blog, archive & search.', 'influencer-internship' ),
             'type'       => 'radio',
            'choices'     => array(
                'default-layout'=> esc_html__('layout one','influencer-internship'),
                'blog1-layout'   => esc_html__('layout two','ifluencer-internship'),
                'blog2-layout'   => esc_html__('layout three','ifluencer-internship'),
            )
        ) 
    );
 
    $wp_customize->add_section( 
        'single_layout',
         array(
            'capability'  => 'edit_theme_options',
            'title'       => esc_html__( 'Single Layout', 'influencer-internship' ),
            'panel'       => 'layout_settings',
            'priority'    => 40,
        ) 
    );
    
    /** Blog layout */
    $wp_customize->add_setting( 
        'layout_single', 
        array(
            'default'           => 'single-layout',
            'sanitize_callback' => 'influencer_internship_sanitize_radio'
        ) 
    );
    
    $wp_customize->add_control(
    'layout_single',
        array(
            'section'     => 'single_layout',
            'label'       => esc_html__( 'Choose Blog Layout', 'influencer-internship' ),
            'description' => esc_html__( 'This is the general layout for blog, archive & search.', 'influencer-internship' ),
                'type'       => 'radio',
            'choices'     => array(
                'single-layout'    => esc_html__('layout one','influencer-internship'),
                'single1-layout'   => esc_html__('layout two','influencer-internship'),
                'single2-layout'   => esc_html__('layout three','influencer-internship'),
            )
        ) 
    );

}
add_action( 'customize_register', 'influencer_internship_layout_customizer_panel' );
 function influencer_internship_sanitize_radio( $input, $setting) {
                                // Ensure input is a slug
	$input = sanitize_key( $input );
	
	// Get a list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;
	
	// If the input is a valid key, return it; otherwise, return the default.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

 }