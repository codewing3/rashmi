<?php
function influencer_internship_blog_page_banner($wp_customize){
    $wp_customize->add_panel(
        'blog_page_settings',
        array(
            'priority'   => 80,
            'capability'  => 'edit_theme_options',
            'title'       => esc_html__('Blog Page Settings','influnencer-internship'),
        )
    );

 $wp_customize->add_section(
        'blog_page_banner_image',
        array(
         'priority'    => 20,
         'title'       => esc_html__( 'Blog Banner Image', 'influencer-internship' ),
         'panel'     => 'blog_page_settings'
       )
     );

    $wp_customize->add_setting( 
        'blog_banner_image', 
    array(
        'default'                       => get_theme_file_uri('/images/header-bg.jpg'),     
        'sanitize_callback'             =>  'esc_url_raw',
    )
  );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'blog_banner_image',
            array(
                'label'      => esc_html__( 'upload image', 'influencer-internship' ),
                'section'    => 'blog_page_banner_image',
            )
        )
    );
    $wp_customize->add_section(
        'blog_page_banner_title',
        array(
         'priority'    => 30,
         'title'       => esc_html__( 'Blog Banner Title', 'influencer-internship' ),
         'panel'     => 'blog_page_settings',
       )
     );

     $wp_customize->add_setting(
        'blog_banner_title',
        array(
            'default'           => esc_html__( 'Content Marketing Articles','influencer-internship' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
        
    $wp_customize->add_control(
        'blog_banner_title',
        array(
            'section'     => 'blog_page_banner_title',
            'label'       => esc_html__( 'Blog SubTitle', 'influencer-internship' ),
            'description' => esc_html__( 'Set the subtitle for Blog.', 'influencer-internship' ),
            'type'        => 'text',
        )
    );
}
add_action('customize_register', 'influencer_internship_blog_page_banner');