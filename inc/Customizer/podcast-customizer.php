<?php
function influencer_internship_podcast_customizer_panel( $wp_customize){
    $wp_customize->add_panel( 
        'podcast_settings',
         array(
            'priority'    => 60,
            'capability'  => 'edit_theme_options',
            'title'       => esc_html__( 'Podcast Settings', 'influencer-internship' ),
            'description' => esc_html__( 'Customize Podcast Settings', 'influencer-internship' ),
        ) 
    );

    $wp_customize->add_section(
        'podcast_banner_settings',
        array(
         'priority'    => 20,
         'title'       => esc_html__( 'Podcast Banner Settings', 'influencer-internship' ),
         'panel'     => 'podcast_settings',
       )
     );
 
     $wp_customize->add_setting( 
         'podcast_banner_image', 
     array(
         'default'                       => get_template_directory_uri(). '/images/header-bg.jpg',     
         'sanitize_callback'             =>  'esc_url_raw',
       )
    );
    
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'banner_image',
            array(
                'label'      => __( 'upload image', 'influencer-internship' ),
                'section'    => 'podcast_banner_settings',
            )
        )
    );

    $wp_customize->add_setting(
        'podcast_banner_title',
        array(
         'default'   =>       esc_html__('Podcast','influencer-internship'),
         'sanitize_callback'=> 'sanitize_text_field',
       )
     );

    $wp_customize->add_control(
        'podcast_banner_title',
        array(
            'section'      => 'podcast_banner_settings',
            'label'        => esc_html__('Podcast title','influencer-internship'),
            'description'  => esc_html__('set the title for podcast', 'influencer-internship'),
            'type'         => 'text',
        )
    );
    
}
add_action('customize_register','influencer_internship_podcast_customizer_panel') ;