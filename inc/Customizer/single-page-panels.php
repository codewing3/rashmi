<?php
function influencer_internship_single_customizer_panel( $wp_customize){
    $wp_customize->add_panel( 
        'single_page_settings',
         array(
            'priority'    => 50,
            'capability'  => 'edit_theme_options',
            'title'       => esc_html__( 'Single Page Settings', 'influencer-internship' ),
            'description' => esc_html__( 'Customize Single Page Settings', 'influencer-internship' ),
        ) 
    );

    $wp_customize->add_section(
        'single_page_banner_image',
        array(
         'priority'    => 20,
         'capability'  => 'edit_theme_options',
         'title'       => esc_html__( 'Banner Settings', 'influencer-internship' ),
         'panel'     => 'single_page_settings'
       )
     );
 
     $wp_customize->add_setting( 
         'single_banner_image', 
     array(
         'default'                       => get_theme_file_uri('/images/single-header-bg.jpg'),     
         'sanitize_callback'             =>  'esc_url_raw',
       )
    );
 
     $wp_customize->add_control(
         new WP_Customize_Image_Control(
             $wp_customize,
             'single_banner_image',
             array(
                 'label'      => __( 'upload image', 'influencer-internship' ),
                 'section'    => 'single_page_banner_image',
             )
         )
     );
     $wp_customize->add_section(
        'singlepage_banner_title',
        array(
         'priority'    => 30,
         'title'       => esc_html__( 'Singlepage Banner Title', 'influencer-internship' ),
         'panel'     => 'single_page_settings',
       )
     );

     $wp_customize->add_setting(
        'banner_title',
        array(
            'default'           => esc_html__( 'Big Questions From Content Marketing World to Ask','influencer-internship' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
        
    $wp_customize->add_control(
        'banner_title',
        array(
            'section'     => 'singlepage_banner_title',
            'label'       => esc_html__( 'Blog SubTitle', 'influencer-internship' ),
            'description' => esc_html__( 'Set the subtitle for Blog.', 'influencer-internship' ),
            'type'        => 'text',
        )
    );  
}
    add_action( 'customize_register', 'influencer_internship_single_customizer_panel' );


    

