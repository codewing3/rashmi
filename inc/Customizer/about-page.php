<?php
function influencer_internship_about_page_banner($wp_customize){
    $wp_customize->add_panel(
        'about_page_settings',
        array(
            'priority'   => 70,
            'capability'  => 'edit_theme_options',
            'title'       => esc_html__('About Page Settings','influnencer-internship'),
        )
    );

 $wp_customize->add_section(
        'about_page_banner_image',
        array(
         'priority'    => 20,
         'title'       => esc_html__( 'About Banner Image', 'influencer-internship' ),
         'panel'     => 'about_page_settings'
       )
     );

    $wp_customize->add_setting( 
        'about_banner_image', 
    array(
        'default'                       => get_theme_file_uri('/images/header-bg.jpg'),     
        'sanitize_callback'             =>  'esc_url_raw',
    )
  );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'about_banner_image',
            array(
                'label'      => esc_html__( 'upload image', 'influencer-internship' ),
                'section'    => 'about_page_banner_image',
            )
        )
    );

     $wp_customize->add_setting(
        'about_banner_title',
        array(
            'default'           => esc_html__( 'About Us','influencer-internship' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
        
    $wp_customize->add_control(
        'about_banner_title',
        array(
            'section'     => 'about_page_banner_image',
            'label'       => esc_html__( 'About Title', 'influencer-internship' ),
            'description' => esc_html__( 'Set the subtitle for About', 'influencer-internship' ),
            'type'        => 'text',
        )
    );

    $wp_customize->add_section(
        'story_settings',
        array(
            'priority'   => 50,
            'capability'  => 'edit_theme_options',
            'title'       => esc_html__('Story Setting','influnencer-internship'),
            'panel'       => 'about_page_settings',
        )
    );

    $wp_customize->add_setting(
        'story_sub_title',
        array(
            'default'           => esc_html__( 'Things Behind Our Success','influencer-internship' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
        
    $wp_customize->add_control(
        'story_sub_title',
        array(
            'section'     => 'story_settings',
            'label'       => esc_html__( 'story subtitle', 'influencer-internship' ),
            'description' => esc_html__( 'Set the subtitle for story', 'influencer-internship' ),
            'type'        => 'textarea',
        )
    );

    $wp_customize->add_setting(
        'story_title',
        array(
            'default'           => esc_html__( 'Our Story','influencer-internship' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
        
    $wp_customize->add_control(
        'story_title',
        array(
            'section'     => 'story_settings',
            'label'       => esc_html__( 'story Title', 'influencer-internship' ),
            'description' => esc_html__( 'Set the title for Blog.', 'influencer-internship' ),
            'type'        => 'text',
        )
    );

    $wp_customize->add_setting( 
        'story_image', 
    array(
        'default'                       => get_theme_file_uri('/images/story-img.jpg'),     
        'sanitize_callback'             =>  'esc_url_raw',
    )
  );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'story_image',
            array(
                'label'      => esc_html__( 'upload image', 'influencer-internship' ),
                'section'    => 'story_settings',
            )
        )
    );


}
add_action('customize_register', 'influencer_internship_about_page_banner');