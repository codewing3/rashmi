<?php 
function influencer_internship_latest_news( $wp_customize){
    $wp_customize->add_section(
        'latest_news_settings',
        array(
            'priority' => 30,
            'title'    => esc_html__( 'Latest News', 'influencer-internship' ),
            'panel'    => 'front_page_settings',
        )
    );

    //title
    $wp_customize->add_setting(
        'latest_news_title',
        array(
            'default'           => esc_html__( 'Latest News','influencer-internship' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
        
    $wp_customize->add_control(
        'latest_news_title',
        array(
            'section'     => 'latest_news_settings',
            'label'       => esc_html__( 'Blog Title', 'influencer-internship' ),
            'description' => esc_html__( 'Set the title for Blog.', 'influencer-internship' ),
            'type'        => 'text',
        )
    );
    $wp_customize->add_setting(
        'latest_news_subtitle',
        array(
            'default'           => esc_html__( 'Tips for getting things done','influencer-internship' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
        
    $wp_customize->add_control(
        'latest_news_subtitle',
        array(
            'section'     => 'latest_news_settings',
            'label'       => esc_html__( 'Blog SubTitle', 'influencer-internship' ),
            'description' => esc_html__( 'Set the subtitle for Blog.', 'influencer-internship' ),
            'type'        => 'text',
        )
    );
    $wp_customize->add_setting(
        'more_from_blog_title',
        array(
            'default'           => esc_html__( 'More From The Blog','influencer-internship' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
        
    $wp_customize->add_control(
        'more_from_blog_title',
        array(
            'section'     => 'latest_news_settings',
            'label'       => esc_html__( 'More From Blog Title', 'influencer-internship' ),
            'description' => esc_html__( 'Set the title for button.', 'influencer-internship' ),
            'type'        => 'text',
        )
    );

}
add_action( 'customize_register','influencer_internship_latest_news');