<?php
function influencer_internship_setup( ){
    load_theme_textdomain( 'influencer-internship', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );


    /*
    * Let WordPress manage the document title.
    * By adding theme support, we declare that this theme does not use a
    * hard-coded <title> tag in the document head, and expect WordPress to
    * provide it for us.
    */
    add_theme_support( 'title-tag' );


    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(
        array(
            'primary' =>__( 'Primary', 'influencer-internship' ),
            'footer' =>__( 'Footer','influencer-internship'),
        )
    );

    add_theme_support(
    'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'style',
            'script',
        )
    );

    // Set up the WordPress core custom background feature.
    add_theme_support(
        'custom-background',
        apply_filters(
            'influencer_internship_custom_background_args',
            array(
                'default-color' => 'ffffff',
                'default-image' => '',
            )
        )
    );
    
    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );

    /** 
    * Add support for core custom logo.
    *
    * @link https://codex.wordpress.org/Theme_Logo
    */


    add_theme_support(
        'custom-logo',
        array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        )
    );

    add_theme_support( 
        'custom-header',
        apply_filters(
            'ultraviolet_internship_custom_header_args',
            array(
                'default-image' => esc_url( get_template_directory_uri() .'/images/banner-img.jpg'),
                'video'          => true,
                'width'          => 1920,
                'height'         => 999,
                'header-text'    => false,
            )
        )
    );

    add_theme_support('post-thumbnails');

   add_image_size('influencer_internship_news', 370, 499, true);
   add_image_size('influencer-internship_related-article', 390, 232, true );

}
add_action('after_setup_theme', 'influencer_internship_setup');