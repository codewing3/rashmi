	<footer class="site-footer" itemscope itemtype="http://schema.org/WPFooter">
		<div class="top-footer">
			<div class="cm-wrapper">
				<?php 
					if( is_active_sidebar('footer-area')){
						dynamic_sidebar( 'footer-area');
					}
				?>
				<?php 
					if( is_active_sidebar('footer-area-2')){
						dynamic_sidebar( 'footer-area-2');
					}
				?>
				<?php
					if( is_active_sidebar('footer-area-3')){
						dynamic_sidebar('footer-area-3');
					}
				?>
				<?php
				if( is_active_sidebar('footer-area-4')){
					dynamic_sidebar('footer-area-4');
				}
				?>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="cm-wrapper">
				<?php
					$influencer_internship_copyright= get_theme_mod('influencer_internship_copyright',__( '© Copyright 2017, Content Marketing. All Rights Reserved. Theme by Rara Theme. Powered by WordPress') );
				?>
				<?php if ($influencer_internship_copyright): ?>
				<div class="copyright"><?php echo wp_kses_post( wpautop( $influencer_internship_copyright ) ); ?></div>
				<?php  else : ?>
					<?php
						esc_html_e('Copyright &copy;', 'influencer-internship');
						echo date_i18n( esc_html__('Y', 'influencer-internship'));
						echo'<a href="' . esc_url( home_url('/') ) .'">' .esc_html( get_bloginfo( 'name')).'</a>.';
					?>
				<?php endif; ?>
				<div class="scroll-to-top">
					<span><?php esc_html_e('back to top','influencer-internship'); ?> <img src="<?php echo esc_url(get_template_directory_uri()	.'/images/scroll-top-arrow.png')?>" alt="<?php esc_attr_e('scroll to top','influencer-internship'); ?>"></span>
				</div>
			</div>
		</div>
	</footer><!-- .site-footer -->
</div>
<?php wp_footer();?>
</body>
</html>